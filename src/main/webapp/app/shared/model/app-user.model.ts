import { IWallet } from 'app/shared/model/wallet.model';

export interface IAppUser {
  id?: number;
  wallet?: IWallet | null;
}

export const defaultValue: Readonly<IAppUser> = {};
