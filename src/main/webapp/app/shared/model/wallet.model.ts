import { IUser } from 'app/shared/model/user.model';
import { ITransaction } from 'app/shared/model/transaction.model';

export interface IWallet {
  id?: number;
  accountNumber?: string | null;
  accountBalance?: number | null;
  user?: IUser | null;
  transactions?: ITransaction[] | null;
}

export const defaultValue: Readonly<IWallet> = {};
