import dayjs from 'dayjs';
import { IWallet } from 'app/shared/model/wallet.model';

export interface ITransaction {
  id?: number;
  amount?: number | null;
  recipientAccountNumber?: string | null;
  recipientAccountName?: string | null;
  transactionStatus?: string | null;
  date?: string | null;
  wallets?: IWallet | null;
}

export const defaultValue: Readonly<ITransaction> = {};
