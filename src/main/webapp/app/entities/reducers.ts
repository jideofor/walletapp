import transaction from 'app/entities/transaction/transaction.reducer';
import wallet from 'app/entities/wallet/wallet.reducer';
import appUser from 'app/entities/app-user/app-user.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  transaction,
  wallet,
  appUser,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
