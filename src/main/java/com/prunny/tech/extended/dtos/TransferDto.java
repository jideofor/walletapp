package com.prunny.tech.extended.dtos;

import java.math.BigDecimal;

public class TransferDto {
    private BigDecimal amount;
    private String recipientAccountNumber;

    public TransferDto(BigDecimal amount, String recipientAccountNumber) {
        this.amount = amount;
        this.recipientAccountNumber = recipientAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    public void setRecipientAccountNumber(String recipientAccountNumber) {
        this.recipientAccountNumber = recipientAccountNumber;
    }

    @Override
    public String toString() {
        return "TransferDto{" +
            "amount=" + amount +
            ", recipientAccountNumber='" + recipientAccountNumber + '\'' +
            '}';
    }
}
