package com.prunny.tech.extended.controller;

import com.prunny.tech.extended.dtos.ApiResponse;
import com.prunny.tech.extended.dtos.TransferDto;
import com.prunny.tech.extended.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/wallet")
@RestController
public class ExtendedWalletController {
    @Autowired
    private WalletService walletService;
    @PostMapping("/transfer")
    public ResponseEntity<ApiResponse<?>> transferFunds(@RequestBody TransferDto transferDto){
        return new ResponseEntity<>(walletService.transferFunds(transferDto), HttpStatus.OK);
    }
}
