package com.prunny.tech.extended.service;

import com.prunny.tech.extended.dtos.ApiResponse;
import com.prunny.tech.extended.dtos.TransferDto;

public interface WalletService {
    ApiResponse<?> transferFunds(TransferDto transferDto);
}
