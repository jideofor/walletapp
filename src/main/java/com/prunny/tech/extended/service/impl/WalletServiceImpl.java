package com.prunny.tech.extended.service.impl;

import com.prunny.tech.domain.Transaction;
import com.prunny.tech.domain.User;
import com.prunny.tech.domain.Wallet;
import com.prunny.tech.extended.dtos.ApiResponse;
import com.prunny.tech.extended.dtos.TransferDto;
import com.prunny.tech.extended.repository.ExtendedWalletRepository;
import com.prunny.tech.extended.service.WalletService;
import com.prunny.tech.extended.utils.UserUtils;
import com.prunny.tech.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;
@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    private UserUtils userUtils;

    @Autowired
    private ExtendedWalletRepository walletRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public ApiResponse<?> transferFunds(TransferDto transferDto) {
        User loggeInUser = userUtils.getCurrentUser();
        Wallet userWallet;
        Optional<Wallet> wallet = walletRepository.findByUser(loggeInUser);

        if(wallet.isEmpty()){
            userWallet = new Wallet();
            userWallet.setUser(loggeInUser);
            userWallet.accountBalance(BigDecimal.ZERO);
            userWallet.accountNumber("1234"+Math.random());
            walletRepository.save(userWallet);
        }else{
            userWallet = wallet.get();
        }

        Transaction transaction = new Transaction();
        transaction.setWallets(userWallet);
        transaction.setAmount(transferDto.getAmount());
        transaction.setRecipientAccountName("hard coded Name");
        transaction.setTransactionStatus("PENDING");
        transaction.setRecipientAccountNumber(transferDto.getRecipientAccountNumber());
        transactionRepository.save(transaction);

        userWallet.setAccountBalance(userWallet.getAccountBalance().subtract(transferDto.getAmount()));
        walletRepository.save(userWallet);

        return new ApiResponse<>(true, "Transfer made successfully",transaction);
    }

//    public WalletServiceImpl(UserUtils userUtils, ExtendedWalletRepository walletRepository, TransactionRepository transactionRepository) {
//        this.userUtils = userUtils;
//        this.walletRepository = walletRepository;
//        this.transactionRepository = transactionRepository;
//    }

}
