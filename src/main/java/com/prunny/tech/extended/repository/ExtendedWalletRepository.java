package com.prunny.tech.extended.repository;

import com.prunny.tech.domain.User;
import com.prunny.tech.domain.Wallet;
import com.prunny.tech.repository.WalletRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExtendedWalletRepository extends WalletRepository {
    Optional<Wallet> findByUser(User user);
}
