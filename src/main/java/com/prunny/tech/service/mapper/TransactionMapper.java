package com.prunny.tech.service.mapper;

import com.prunny.tech.domain.Transaction;
import com.prunny.tech.domain.Wallet;
import com.prunny.tech.service.dto.TransactionDTO;
import com.prunny.tech.service.dto.WalletDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Transaction} and its DTO {@link TransactionDTO}.
 */
@Mapper(componentModel = "spring")
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {
    @Mapping(target = "wallets", source = "wallets", qualifiedByName = "walletId")
    TransactionDTO toDto(Transaction s);

    @Named("walletId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    WalletDTO toDtoWalletId(Wallet wallet);
}
