package com.prunny.tech.service.mapper;

import com.prunny.tech.domain.User;
import com.prunny.tech.domain.Wallet;
import com.prunny.tech.service.dto.UserDTO;
import com.prunny.tech.service.dto.WalletDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Wallet} and its DTO {@link WalletDTO}.
 */
@Mapper(componentModel = "spring")
public interface WalletMapper extends EntityMapper<WalletDTO, Wallet> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    WalletDTO toDto(Wallet s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);
}
