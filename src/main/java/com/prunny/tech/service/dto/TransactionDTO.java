package com.prunny.tech.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.prunny.tech.domain.Transaction} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TransactionDTO implements Serializable {

    private Long id;

    private BigDecimal amount;

    private String recipientAccountNumber;

    private String recipientAccountName;

    private String transactionStatus;

    private LocalDate date;

    private WalletDTO wallets;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    public void setRecipientAccountNumber(String recipientAccountNumber) {
        this.recipientAccountNumber = recipientAccountNumber;
    }

    public String getRecipientAccountName() {
        return recipientAccountName;
    }

    public void setRecipientAccountName(String recipientAccountName) {
        this.recipientAccountName = recipientAccountName;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public WalletDTO getWallets() {
        return wallets;
    }

    public void setWallets(WalletDTO wallets) {
        this.wallets = wallets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionDTO)) {
            return false;
        }

        TransactionDTO transactionDTO = (TransactionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, transactionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TransactionDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", recipientAccountNumber='" + getRecipientAccountNumber() + "'" +
            ", recipientAccountName='" + getRecipientAccountName() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", date='" + getDate() + "'" +
            ", wallets=" + getWallets() +
            "}";
    }
}
