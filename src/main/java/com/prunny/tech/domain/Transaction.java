package com.prunny.tech.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "amount", precision = 21, scale = 2)
    private BigDecimal amount;

    @Column(name = "recipient_account_number")
    private String recipientAccountNumber;

    @Column(name = "recipient_account_name")
    private String recipientAccountName;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "date")
    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "user", "transactions" }, allowSetters = true)
    private Wallet wallets;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Transaction id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public Transaction amount(BigDecimal amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRecipientAccountNumber() {
        return this.recipientAccountNumber;
    }

    public Transaction recipientAccountNumber(String recipientAccountNumber) {
        this.setRecipientAccountNumber(recipientAccountNumber);
        return this;
    }

    public void setRecipientAccountNumber(String recipientAccountNumber) {
        this.recipientAccountNumber = recipientAccountNumber;
    }

    public String getRecipientAccountName() {
        return this.recipientAccountName;
    }

    public Transaction recipientAccountName(String recipientAccountName) {
        this.setRecipientAccountName(recipientAccountName);
        return this;
    }

    public void setRecipientAccountName(String recipientAccountName) {
        this.recipientAccountName = recipientAccountName;
    }

    public String getTransactionStatus() {
        return this.transactionStatus;
    }

    public Transaction transactionStatus(String transactionStatus) {
        this.setTransactionStatus(transactionStatus);
        return this;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Transaction date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Wallet getWallets() {
        return this.wallets;
    }

    public void setWallets(Wallet wallet) {
        this.wallets = wallet;
    }

    public Transaction wallets(Wallet wallet) {
        this.setWallets(wallet);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", recipientAccountNumber='" + getRecipientAccountNumber() + "'" +
            ", recipientAccountName='" + getRecipientAccountName() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", date='" + getDate() + "'" +
            "}";
    }
}
